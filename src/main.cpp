#include <cstdio>
#include <optional>
#include <vector>
#include <string>
#include <endian.h>
#include <openssl/bio.h>
#include <openssl/evp.h>
#include <openssl/bn.h>
#include <openssl/rsa.h>
#include <openssl/sha.h>
#include <openssl/err.h>
#include <fmt/format.h>
#include "CLI11.hpp"
#include "httplib.h"

// https://www.foonathan.net/2020/09/move-forward/
#define mov(...) static_cast<std::remove_reference_t<decltype(__VA_ARGS__)>&&>(__VA_ARGS__)
#define fwd(...) static_cast<decltype(__VA_ARGS__)&&>(__VA_ARGS__)

//////////////////////////////////
// Common defines
//////////////////////////////////
static constexpr std::string_view gUserAgent{"Android-Finsky/30.2.18-21 (api=3,versionCode=83021810,sdk=31,device=r9q,hardware=qcom,product=r9qxeea,platformVersionRelease=12,model=SM-G990B,buildId=SP1A.210812.016)"};
static constexpr std::string_view gBaseURL{"https://android.clients.google.com/"};
static constexpr std::string_view gGooglePublicKey{"AAAAgMom/1a/v0lblO2Ubrt60J2gcuXSljGFQXgcyZWveWLEwo6prwgi3iJIZdodyhKZQrNWp5nKJ3srRXcUW+F1BD3baEVGcmEgqaLZUNBjm057pKRI16kB0YppeGx5qIQ5QjKzsR8ETQbKLNWgRY0QRNVz34kMJR3P/LgHax/6rmf5AAAAAwEAAQ=="};

struct ApplicationState
{
	std::vector<std::string> appids;
};

// Taken from: https://stackoverflow.com/a/47981510
namespace {
	struct BIOFreeAll { void operator()(BIO* p) { BIO_free_all(p);  }  };
}
std::vector<std::byte> Base64Decode(std::string_view b64string)
{
	std::unique_ptr<BIO,BIOFreeAll> b64(BIO_new(BIO_f_base64()));
	BIO_set_flags(b64.get(), BIO_FLAGS_BASE64_NO_NL);
	BIO* source = BIO_new_mem_buf(b64string.data(), -1); // read-only source
	BIO_push(b64.get(), source);
	const int maxlen = b64string.size() / 4 * 3 + 1;
	std::vector<std::byte> decoded(maxlen);
	const int len = BIO_read(b64.get(), decoded.data(), maxlen);
	decoded.resize(len);
	return decoded;
}

std::string Base64Encode(std::vector<std::byte> data)
{
	std::unique_ptr<BIO,BIOFreeAll> b64(BIO_new(BIO_f_base64()));
	BIO_set_flags(b64.get(), BIO_FLAGS_BASE64_NO_NL);
	BIO* sink = BIO_new(BIO_s_mem());
	BIO_push(b64.get(), sink);
	BIO_write(b64.get(), data.data(), data.size());
	BIO_flush(b64.get());
	const char* encoded;
	const long len = BIO_get_mem_data(sink, &encoded);
	return std::string(encoded, len);
}

std::optional<int> ParseCommandLine(ApplicationState *state, int argc, char **argv)
{
	CLI::App cmdline{"Google Play Store APK Downloader"};

	auto fmt = std::make_shared<CLI::Formatter>();
	fmt->label("REQUIRED", "\033[91mREQUIRED\033[0m");
	fmt->label("Usage", "\033[94mUsage\033[0m");
	fmt->label("Options", "\033[95mOPTIONS\033[0m");
	fmt->label("OPTIONS", "\033[95mOPTIONS\033[0m");
	fmt->label("SUBCOMMAND", "\033[92mSUBCOMMAND\033[0m");
	fmt->label("SUBCOMMANDS", "\033[92mSUBCOMMANDS\033[0m");
	cmdline.formatter(fmt);

	// TODO: move the flag variables
	bool version{false};

	cmdline.add_flag("--version", version, "Print version and license information");
	cmdline.add_option("appids", state->appids, "App IDs to download from the Google Play store");

	try
	{
		cmdline.parse(argc, argv);
	}
	catch (const CLI::ParseError &e)
	{
		return std::make_optional(cmdline.exit(e));
	}

	if (version)
	{
		// TODO: print version
		printf("lol no version 4 u\n");
		return std::make_optional(EXIT_SUCCESS);
	}

	return std::nullopt;
}

class GPlayAPI
{
	std::string aastoken_;
	std::string device_id_;
	std::string user_agent_;

	std::string locale_;
	std::string timezone_;

public:
	GPlayAPI(std::string token, std::string device_id, std::string user_agent) : aastoken_(mov(token)), device_id_(mov(device_id)), user_agent_(mov(user_agent))
	{
	}

	// Not copyable
	GPlayAPI(const GPlayAPI&) = delete;
	GPlayAPI &operator=(const GPlayAPI&) = delete;

	// Moveable
	constexpr GPlayAPI(GPlayAPI &&rhs) noexcept
	{
		if (&rhs == this) [[unlikely]]
			return;
		
		this->aastoken_ = mov(rhs.aastoken_);
		this->device_id_ = mov(rhs.device_id_);
		this->user_agent_ = mov(rhs.user_agent_);
	}
	
	constexpr GPlayAPI &operator=(GPlayAPI &&rhs) noexcept
	{
		if (&rhs == this) [[unlikely]]
			return *this;

		this->aastoken_ = mov(rhs.aastoken_);
		this->device_id_ = mov(rhs.device_id_);
		this->user_agent_ = mov(rhs.user_agent_);

		return *this;
	}

	std::map<std::string, std::string> GetHeaders(std::string device_id, std::string user_agent, std::string locale) const noexcept
	{
		return std::map<std::string, std::string>{
			{"Accept-Language", mov(locale)},
			{"Authorization", "GoogleLogin auth={}"},
			{"X-DFE-Enabled-Experiments", "cl:billing.select_add_instrument_by_default"},
			{"X-DFE-Unsupported-Experiments", "nocache:billing.use_charging_poller,market_emails,buyer_currency<Plug>PeepOpenrod_baseline,checkin.set_asset_paid_app_field,shekel_test,content_ratings,buyer_currency_in_app,nocache:encrypted_apk,recent_changes"},
			{"X-DFE-Device-Id", mov(device_id)},
			{"X-DFE-Client-Id", "am-android-google"},
			{"User-Agent", mov(user_agent)},
			{"X-DFE-SmallestScreenWidthDp", "320"},
			{"X-DFE-Filter-Level", "3"},
			{"Host", "android.clients.google.com"},
			{"Content-Type", "application/x-www-form-urlencoded; charset=UTF-8"} // TODO: handle content type.
		};
	}


	static std::string EncryptPassword(std::string_view login, std::string_view password)
	{
		// Structure of the binary key:
		//
		// *-------------------------------------------------------*
		// | modulus_length | modulus | exponent_length | exponent |
		// *-------------------------------------------------------*
		// modulus_length and exponent_length are uint32

		std::vector<std::byte> buffer;

		auto bkey = Base64Decode(gGooglePublicKey);

		BIGNUM *modulus = BN_new();
		BIGNUM *exponent = BN_new();
		if (!modulus)
			throw std::bad_alloc();

		if (!exponent)
		{
			// Free modulus memory first
			BN_free(modulus);
			throw std::bad_alloc();
		}

		unsigned char *data = reinterpret_cast<unsigned char*>(bkey.data());

		// Read modulus_length, it's big-endian so it has to be changed.
		uint32_t modulus_len = be32toh(*reinterpret_cast<uint32_t*>(data));
		// move ptr
		data += sizeof(uint32_t);
		// safety check
		assert(modulus_len + sizeof(uint32_t) <= bkey.size() && "modulus length is invalid!");
		// read modulus
		modulus = BN_bin2bn(data, modulus_len, modulus);
		// move ptr
		data += modulus_len;
		// get explonent length, convert from big-endian
		uint32_t exponent_len = be32toh(*reinterpret_cast<uint32_t*>(data));
		// move ptr
		data += sizeof(uint32_t);
		// Make sure we didn't go past the array
		assert(modulus_len + (sizeof(uint32_t) * 2) + exponent_len <= bkey.size() && "public key invalid length");
		// Decode exponent from binary
		exponent = BN_bin2bn(data, exponent_len, exponent);

		// Calculate SHA-1 of the public key
		// TODO: this could be constexpr at some point
		// since the signature will always be the same.
		SHA_CTX ctx;
		std::vector<unsigned char> digest(SHA_DIGEST_LENGTH);
		SHA1_Init(&ctx);
		SHA1_Update(&ctx, bkey.data(), bkey.size());
		SHA1_Final(digest.data(), &ctx);

		// Store the fingerprint of the key?
		std::byte fingerprint[5]{std::byte{0x00}};
		memcpy(fingerprint+1, digest.data(), sizeof(fingerprint)-1);

		RSA *rsa = RSA_new();
		RSA_set0_key(rsa, mov(modulus), mov(exponent), nullptr);

		// Data to be encrypted
		std::vector<unsigned char> to_be_encrypted;
		to_be_encrypted.insert(to_be_encrypted.end(), login.data(), login.data() + login.size());
		to_be_encrypted.push_back('\0');
		to_be_encrypted.insert(to_be_encrypted.end(), password.data(), password.data() + password.size());

		// RSA encrypt using RSA/ECB/OAEPWITHSHA1ANDMGF1PADDING
		size_t rsa_buffer_sz = RSA_size(rsa);
		unsigned char *rsa_buffer = new unsigned char[rsa_buffer_sz];

		if (RSA_public_encrypt(to_be_encrypted.size(), to_be_encrypted.data(), rsa_buffer, rsa, RSA_PKCS1_OAEP_PADDING))
		{
			ERR_print_errors_fp(stderr);
			throw std::runtime_error("Could not encrypt data with public key");
		}

		// base64 encode
		std::vector<std::byte> encrypted_data(fingerprint, fingerprint+sizeof(fingerprint));
		encrypted_data.insert(encrypted_data.end(), reinterpret_cast<std::byte*>(rsa_buffer), reinterpret_cast<std::byte*>(rsa_buffer+rsa_buffer_sz));

		// Free memory, nothing is needed anymore
		RSA_free(rsa);

		return Base64Encode(mov(encrypted_data));
	}
};


int main(int argc, char** argv)
{
	ApplicationState state;
	auto res = ParseCommandLine(&state, argc, argv);

	if (res)
		return res.value();

	if (state.appids.empty())
		fmt::print("You must specify an app id to download\n");
	else 
	{
		for (auto &&it : state.appids)
		{
			fmt::print("Package ID: {}\n", it);
		}
	}

	return 0;
}

APKCLI
===

The Google Play store APK command line downloader.

FAQ
===

What is this?
---

A tool to download APKs directly from the Google Play store instead of some sketchy apk website that wants you to install their app and force you into fake download timers while they "find" a mirror.

Why is this a thing?
---

If you ever needed to reverse engineer an APK then you obviously have to start with the APK. If you have to download it from some random website, how do you know it's the same APK?

What was your motivation for starting this?
---

Originally, I was planning to reverse engineer my banking app so I knew how much money I had in my accounts but I needed the APK. All the sketchy APK websites didn't have it archived so my only option was downloading via play store directly. Downloading the APK from my phone felt like a hack and this was not the first time I needed an APK from google so I decided to make it a dedicated tool.

How do I authenticate to google?
---

Unfortunately, you must setup your own [AuroraOSS token dispenser](https://github.com/whyorean/AuroraDispenser). I may create a C++ version of this token dispenser to make deployment and usage easier but ultimately you will have to figure this out in your own time. 

Keep in mind that it's a violation of Google's Terms of Service to use 3rd party applications to access the play store API and they reserve the right to ban your account at any time for doing so. It's strongly recommended you create throw-away google accounts for your token dispenser. 

Also keep in mind that it may be rude to use AuroraStore's token dispenser (and a violation of their usage agreement based on a note in their group) so it is recommended you setup your own accounts and token dispenser.


Why is it in C++? Why not use &lt;insert language here&gt;?
---

1. I know C++ the best
2. It's fast
3. get mad.

3rd party libraries used
===

1. [cpp-httplib](https://github.com/yhirose/cpp-httplib): used for making HTTP requests
2. [CLI11](https://github.com/CLIUtils/CLI11): used for fancy command line argument parsing
3. [libfmt](https://fmt.dev/): for message formatting that isn't insane
4. [protobuf](https://developers.google.com/protocol-buffers/): Required Play Store garbage for their API

Project license
===

AGPL licensed.

```
apkcli: The Google Play Store APK downloader
Copyright (C) 2022 Justin Crawford

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
```
